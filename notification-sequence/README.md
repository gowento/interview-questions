The goal of the exercise is to write a small module which will resolve the first active channels for a list of users defined in a given notification (30-60mn max.).

### Definitions:
- A `notification` is an object with an `audienceId` (ie, `576fba01-272a-47cb-9f6b-78b9be8ef904`) and a `sequence` which is an ordered array of channels (ie, `['push', 'email', 'sms']`)
- An `audience` is an array of `user` IDs (for instance `['83ab3105-b9f6-41b8-81dd-0c02914126f1', '003805d5-c0a1-4046-9be5-cd8ecf237bb3', 'c8382352-3e98-40f3-97f2-bd2551289177']`)
- Each `user` has the following properties: `hasActivePass`, `hasActiveEmail`, `hasActivePhone`, meaning this user can be notified using the following channels respectively: `push`, `email`, `sms`
  
### Our REST API:
- base url: `https://api.domain.com`
- authentication: basic auth (username: `interview` / password: `exercise`)
- has the following endpoints available:
  - `notifications` / `audiences` / `users`
  - So, for instance, you can get the detail of the `notification` `576fba01` by executing a GET request over `https://api.domain.com/notifications/576fba01`

### Exercise goal
Using the libraries (API client, async flow management, etc.) you want, please write a small set of methods which will use our fake API to:
- fetch the detail of a notification
- then, fetch the array of user IDs of the notification audience
- then, fetch the detail of each user of the audience
- finally, output for each user the first channel (in the order defined in the notification sequence) available (according to the `hasActive...` properties)

### Example
For example, for the following notification sequence
```['email', 'push']```
and the following users
```
[
  {id: '123', hasActiveEmail: false, hasActivePass: true},
  {id: '456', hasActiveEmail: true, hasActivePass: true},
  {id: '789', hasActiveEmail: true, hasActivePass: false},
]
```
The public method should output the following result (an object where the keys are the user IDs and the values are the channel):
```
{
  '123': 'push',
  '456': 'email',
  '789': 'email'
}
```