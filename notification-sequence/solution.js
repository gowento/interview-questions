import request from 'superagent';
import async from 'async';

// variables (better stored in process.env)
const apiEndPoint = 'http://api.gowento.dev:5000';
const apiUsername = 'gowento';
const apiPassword = 'interview';
const notificationId = 'ciiu7tnfu000w0ayygtyk5v86';

// helper method to query the api (basic auth + timeout)
function query(method, uri, cb) {
  request(method, apiEndPoint + uri)
    .timeout(10000)
    .auth(apiUsername, apiPassword)
    .end((err, res) => cb(err, res ? res.body : null));
}

// methods to access api resources
const getNotification = (id, cb) => query('GET', `/notifications/${id}`, cb);
const getUser = (id, cb) => query('GET', `/users/${id}`, cb);
const getAudienceUsers = (id, cb) => query('GET', `/audiences/${id}/users`, cb);

// method to find first active user channel
function getUserNotificationChannel(sequence, user) {
  return sequence.find(channel => (channel === 'push' && user.hasActivePass)
    || (channel === 'email' && user.hasActiveEmail)
    || (channel === 'sms' && user.hasActivePhone)
  );
}

// algorithm flow
async.waterfall([
  (cb) => getNotification(notificationId, cb),
  (notification, cb) => async.waterfall([
    (cb1) => getAudienceUsers(notification.recipients.audienceIds[0], cb1),
    (userIds, cb1) => async.map(userIds, getUser, cb1),
  ], (err, users) => {
    const result = users.reduce((obj, user) => {
      obj[user.id] = getUserNotificationChannel(notification.sequence, user);
      return obj;
    }, {});
    cb(err, result);
  }),
], console.log);
